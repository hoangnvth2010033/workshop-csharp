﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkShop6
{
    class SupplierTest
    {
        static void Main(string[] args)
        {
            Supplier objSupplier = new Supplier();
            objSupplier.AcceptDetails();

            int id = 0;
            string name = "";

            Console.Write("\nEnter the ID of the supplier :");
            id = Convert.ToInt32(Console.ReadLine());

            objSupplier.DisplayDetails(id);
            name = Console.ReadLine();
            objSupplier.DisplayDetails(name);
            objSupplier.DisplayDetails(id, name);
        }
    }
}
