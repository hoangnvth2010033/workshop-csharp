﻿using System;
using System.Collections.Generic;
using System.Text;

namespace School
{
    // <summary>
    
    class Student
    {
        static void Main(string[] args)
        {
            /* int studentID = 1;
             string studentName = "David George";
             byte age = 18;
             char gender = 'M';
             float percent = 75.50F;
             bool pass = true;

             Console.WriteLine("Student ID : {0}", studentID);
             Console.WriteLine("Student Name : {0}", studentName);
             Console.WriteLine("Age : " + age);
             Console.WriteLine("Gender : " + gender);
             Console.WriteLine("Percentage : {0:F2}", percent);
             Console.WriteLine("Passed : {0}", pass);

             */

            const int percentConst = 100;
            string studentName;
            int english, maths, science;
            float percent = 0.0F;

            Console.Write("Enter name of the student :");
            studentName = Console.ReadLine();

            Console.Write("Enter mark for english :");
            english = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter mark for maths :");
            maths = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter marks for science :");
            science = Convert.ToInt32(Console.ReadLine());

            percent = ((english + maths + science) * percentConst) / 300;

            Console.WriteLine();
            Console.WriteLine("*** Student Details ***\n");
            Console.WriteLine("Student Name :"+ studentName);
            Console.WriteLine("Marks obtained in English : {0}",english);
            Console.WriteLine("Marks obtained in Maths : {0}", maths);
            Console.WriteLine("Marks obtained in Science : {0}", science);
            Console.WriteLine("Percent : {0:F2}", percent);
        }
    }
}
